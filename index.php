<?php
namespace Car;
use Car\Sedan as S;
use Car\Lorry as L;

require "Car/index.php";
$sedan = new S("Premio");
$lorry = new L("Fuso");

$sedan->getModel();
echo "\n";
$lorry->getModel();

